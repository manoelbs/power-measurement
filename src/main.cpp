#include <Arduino.h>
#include <Wire.h>
#include <LiquidCrystal_PCF8574.h>

LiquidCrystal_PCF8574 lcd(0x27);    // lcd(i2c_address)

#define INIT_MIN_UPTIME 65535       // 65535 -> impossible min Uptime
#define BUTTON1 5
#define THR_VOLTAGE 514             // Threshold current in volts (integer)
                                    // (514 =) ((0.5 A * 0.066 V/A + 2,35)*10000-706)/45

unsigned long gTimer = 0;
unsigned long gSecs = 0;
unsigned long gTotalUptime = 0;
unsigned int gAvgUptime = 0;
unsigned int gMinUptime = INIT_MIN_UPTIME;
unsigned int gMaxUptime = 0;
unsigned int gUpCount = 0;
unsigned int gUptime = 0;
unsigned int gAnalogValue = 0;
bool gIsUp = false;

float gMaxCurrent = 0;
float gMaxCurrentSum = 0;
float gCurrent = 0;
unsigned char gSamples = 0;
unsigned long gSampleTimer = 0;

void printTime(unsigned long secs);
void updateDisplay();

float current()
{
    unsigned int raw = analogRead(A1);
    float i = ((raw / 1024.0)*4.66 - 2.33)/0.062;
    if(i > 0.1 || i < -0.1) return i;
    return 0.0;
}

void setup()
{
    Wire.begin();
    Serial.begin(9600);
    Serial.println("\nPower measurement");

    lcd.begin(16, 2);   // begin(cols, lines)
    lcd.setBacklight(255);
    lcd.clear();
    lcd.home();

    gTimer = millis();
    pinMode(BUTTON1, INPUT_PULLUP);

    gSampleTimer = millis();
}

void loop()
{
    gAnalogValue = analogRead(A1);
    float analogValue = (gAnalogValue*45+706)/10000.0;
    Serial.println(analogValue);

    if(current() > gMaxCurrent) gMaxCurrent = current();

    if(millis() - gSampleTimer > 20)
    {
        gSampleTimer = millis();
        gMaxCurrentSum += gMaxCurrent;
        gMaxCurrent = 0;
        ++gSamples;
    }

    bool isUpNow = (gCurrent > 0.5);

    if(gIsUp && !isUpNow)
    {
        if(!isUpNow)
        {
            gUpCount++;
            if(gUptime < gMinUptime) gMinUptime = gUptime;
            if(gUptime > gMaxUptime) gMaxUptime = gUptime;
            gUptime = 0;
        }
    }

    gIsUp = isUpNow;

    if(millis() - gTimer > 1000)
    {
        gTimer = millis();
        ++gSecs;

        if (gIsUp)
        {
            ++gTotalUptime;
            ++gUptime;
        }

        gCurrent = gMaxCurrentSum / gSamples;
        gSamples = 0;
        gMaxCurrentSum = 0;
    }

    updateDisplay();
}

/**
 * Converts seconds to clock notation, and prints to
 * current LCD position.
 *
 * Output:
 *  mm'ss   if less than 90 minutes
 *  hh:mm   otherwise
 *
 * @param secs  time in seconds
 */
void printTime(unsigned long secs)
{
    unsigned int mins = secs / 60;
    unsigned int hours = mins / 60;

    if(mins < 90)
    {
        lcd.print(mins < 10 ? "0":"");
        lcd.print(mins);
        lcd.print("'");
        lcd.print((secs % 60) < 10 ? "0":"");
        lcd.print(secs % 60);
    }
    else
    {
        lcd.print(hours < 10 ? "0":"");
        lcd.print(hours);
        lcd.print("h");
        lcd.print((mins % 60) < 10 ? "0":"");
        lcd.print(mins % 60);
    }
}

/**
 * Updates the display every second, and changes the
 * screen being shown when the button is pressed.
 */
unsigned long gDisplayTimer = 0;
char gScreen = 1;
void updateDisplay()
{
    if(digitalRead(BUTTON1) == LOW)
    {
        delay(100);
        while(digitalRead(BUTTON1) == LOW) ;
        if(++gScreen == 3) gScreen = 1;
        gDisplayTimer = 0;
    }

    if(millis() - gDisplayTimer > 1000)
    {
        if(gScreen == 1)
        {
            lcd.setCursor(0, 0); lcd.print("                ");
            lcd.setCursor(0, 0); lcd.print("R ");
            printTime(gSecs);
            lcd.print(" U ");
            printTime(gTotalUptime);

            lcd.setCursor(0, 1); lcd.print("                ");
            lcd.setCursor(0, 1); lcd.print("I ");
            //lcd.print((gAnalogValue*45+706)/10000.0,3);
            lcd.print(gCurrent);
        }
        else if(gScreen == 2)
        {
            lcd.setCursor(0, 0); lcd.print("                ");
            lcd.setCursor(0, 0); lcd.print("L ");
            printTime(gMinUptime == INIT_MIN_UPTIME ? 0 : gMinUptime);
            lcd.print("  H ");
            printTime(gMaxUptime);

            lcd.setCursor(0, 1); lcd.print("                ");
            lcd.setCursor(0, 1); lcd.print("A ");
            printTime( gUpCount > 0 ? (gTotalUptime / gUpCount) : 0);
            lcd.print("  Cnt ");
            lcd.print(gUpCount);
        }

        gDisplayTimer = millis();
    }
}
